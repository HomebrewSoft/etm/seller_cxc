{
    "name": "CxC Seller",
    "version": "13.0.0.1.0",
    "author": "HomebrewSoft",
    "website": "https://homebrewsoft.dev",
    "license": "LGPL-3",
    "depends": [
        "account_followup",
    ],
    "data": [
        # views
        "views/res_partner.xml",
    ],
}
